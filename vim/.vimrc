"like ZZ to exit and save WW just to save"
nmap WW :w<CR>

nmap <C-s> :w<CR>
imap <C-s> <esc>:w<CR>
nmap <C-d> :wq<CR>
imap <C-d> <esc>:wq<CR>
nmap <C-q> :q!<CR>
imap <C-q> <esc>:q!<CR>

"to change to normale mode press jj"
imap jj <esc> 

