#! /bin/bash

#vboxmanage startvm vmname --type headless
#vboxmanage controlvm vmname acpipowerbutton

if [[ $1 == "start" ]]; then
  vboxmanage startvm atlantis --type headless
fi

if [[ $1 == "stop" ]]; then
  # output goes do /dev/null because of :
  #VBoxManage: error: The machine 'atlantis' is already locked by a session (or being locked or unlocked)
  #VBoxManage: error: Details: code VBOX_E_INVALID_OBJECT_STATE (0x80bb0007), component MachineWrap, interface IMachine, callee nsISupports
  #VBoxManage: error: Context: "LaunchVMProcess(a->session, sessionType.raw(), ComSafeArrayAsInParam(aBstrEnv), progress.asOutParam())" 
  #at line 727 of file VBoxManageMisc.cpp
  #but it stop the vm successfully

  vboxmanage controlvm atlantis acpipowerbutton 2> /dev/null
  echo "Waiting for VM "atlantis" to power off..."
  sleep 2
  echo "VM "atlantis" has been successfully stopped."
fi
