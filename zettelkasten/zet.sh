#! /bin/bash

isosec=$(date +'%Y%m%d%H%M')

cd ~/Dokumente/zet

mkdir $isosec 

cd $isosec/

if [[ $# -eq 0 ]]; then
  touch README.md
else 
  touch $1
fi


if [[ $# -eq 0 ]]; then
  nvim README.md
else 
  nvim $1
fi
